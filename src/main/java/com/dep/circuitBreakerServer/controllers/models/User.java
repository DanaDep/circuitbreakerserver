package com.dep.circuitBreakerServer.controllers.models;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
	private String firstName;
	private String lastname;
	private String email;

	public User(String firstName, String lastname, String email){
		this.firstName = firstName;
		this.lastname = lastname;
		this.email = email;
	}
}
