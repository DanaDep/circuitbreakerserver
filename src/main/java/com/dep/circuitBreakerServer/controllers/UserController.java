package com.dep.circuitBreakerServer.controllers;

import com.dep.circuitBreakerServer.controllers.models.User;
import reactor.core.publisher.Mono;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/server")
public class UserController {

    @GetMapping("/user-data")
    public Mono<User> getUserData(){
        return Mono.just( new User( "Jane", "Doe", "jane.doe@mail.com" ));
    }
}
